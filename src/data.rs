use std::collections::HashMap;
use server::{Server, ServerKind, RirId};
use std::str::FromStr;
use std::fmt;

/// It's just a map of whois keywords to values collected from the response
/// Create it from a server response string.
pub struct Data {
    pub map: HashMap<String, String>,
}

impl Data {

    fn new() -> Data {
        Data{map: HashMap::new()}
    }

    pub fn collect_data(&mut self, src: &str) -> Result<(),()> {
        for line in src.lines() {
            if line.starts_with("#") || line.starts_with("%") || line.trim().is_empty() {
                continue;
            }
            // Split only once, into 2 pieces. The colon ":" is allowed on the right side, the value.
            let mut fields = line.splitn(2, ":");
            let key: String;
            let val: String;
            match fields.next() {
                Some(k) => {
                    key = k.to_string();
                    match fields.next() {
                        Some(v) => {
                            val = v.trim().to_string();
                            self.map.insert(key, val);
                        },
                        // FIXME silently ignore error
                        _ => ()
                    };
                },
                // FIXME silently ignore error
                _ => ()
            };
        }
        Ok(())
    }
    pub fn referral(&self) -> Option<Server> {
        let mut referral = self.map.get("ReferralServer");
        if referral == None {
            referral = self.map.get("referto");
        }
        let _proto: String;
        let domain: String;
        let port: u16;

        match referral {
            Some(url) => {
                match url.parse::<Server>() {
                    Ok(r) => Some(r),
                    Err(_) => None
                }
            },
            None => None,
        }
    }

    /// Deprecating
    /// Use `referral()` instead
    pub fn get_referral(&self) -> Option<Server> {
        log::warn!("Deprecating, use method referral() instead");
        self.referral()
    }

    pub fn parse<T>(&self) -> Result<T, <T as FromData>::Err>
        where T: FromData {
            FromData::from_data(&self)
    }

    pub fn parse_whois<T>(&self, t: &ServerKind) -> Result<T, <T as FromDataThruServerKind>::Err>
        where T: FromDataThruServerKind {
            FromDataThruServerKind::from_data_kind(&self, &t)
    }
}

/// Trait to be implemented from other classes
pub trait FromData {
    type Err;
    fn from_data(s: &Data) -> Result<Self, Self::Err>
        where Self: Sized;
}
pub trait FromDataThruServerKind {
    type Err;
    fn from_data_kind(s: &Data, t: &ServerKind) -> Result<Self, Self::Err>
        where Self: Sized;
}

pub trait FromStrThruServerKind {
    type Err;
    fn from_str_kind(s: &str, t: &ServerKind) -> Result<Self, Self::Err>
        where Self: Sized;
}

pub trait IntoDataThruServerKind {
    fn parse_whois<F: FromStrThruServerKind>(&self, t: &ServerKind) -> Result<F, F::Err>;
}

impl IntoDataThruServerKind for str {
    fn parse_whois<F: FromStrThruServerKind>(&self, t: &ServerKind) -> Result<F, F::Err> {
        FromStrThruServerKind::from_str_kind(self, t)
    }
}

impl FromStrThruServerKind for Data {
    type Err = ParseDataError;
    fn from_str_kind(s: &str, t: &ServerKind) -> Result<Self, Self::Err> {
        let mut whois_data = Data::new();
        let mut collect_data = |delim: &str, comment_prefix: &str| -> Result<(),()> {
            for line in s.lines() {
                if line.starts_with(comment_prefix) || line.trim().is_empty() {
                    continue;
                }
                // Split only once, into 2 pieces. The colon ":" is allowed on the right side, the value.
                let mut fields = line.splitn(2, delim);
                let key: String;
                let val: String;
                match fields.next() {
                    Some(k) => {
                        key = k.to_string();
                        match fields.next() {
                            Some(v) => {
                                val = v.trim().to_string();
                                whois_data.map.insert(key, val);
                            },
                            // FIXME silently ignore error
                            _ => ()
                        };
                    },
                    // FIXME silently ignore error
                    _ => ()
                };
            }
            Ok(())
        };

        let result = match t {
            ServerKind::Rir(ririd) => {
                match ririd {
                    RirId::Afrinic => collect_data(":", "%"),
                    RirId::Apnic   => collect_data(":", "%"),
                    RirId::Arin    => collect_data(":", "#"),
                    RirId::Lacnic  => collect_data(":", "%"),
                    RirId::Ripe    => collect_data(":", "%"),
                }
            }
            ServerKind::Tld => Err(()),
            ServerKind::Generic => Err(()),
        };
        match result {
            Ok(_) => Ok(whois_data),
            Err(_) => Err(ParseDataError),
        }
    }
}

impl FromStr for Data {
    type Err = ParseDataError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {

        let mut whois_data = Data::new();
        if let Err(_e) = whois_data.collect_data(s) {
            return  Err(ParseDataError{});
        }
        Ok(whois_data)
    }
}
// std/error/trait.Error.html
//
#[derive(Debug)]
pub struct ParseDataError;


impl fmt::Display for ParseDataError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(f, "Error while parsing whois input data")
        }
}

impl std::error::Error for ParseDataError {
}
