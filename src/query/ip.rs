use query::FetchServer;
use resolve::resolver::resolve_host;
use std::net::IpAddr;
use ip_map::ManagedByWhoisServer;
use info::Info;
use std::error::Error;
use std::str::FromStr;

use std::net::SocketAddr;
use std::net::TcpStream;
use std::io::Write;
use std::io::Read;
use server::Server;

use query::fetch_with_referrals;
use query::QueryParam;

pub struct IpQuery {}

impl FetchServer for IpQuery {
    fn fetch(s_ip: &str) -> Result<Info, Box<dyn Error>> {
        let ip = IpAddr::from_str(s_ip).expect("BUG! Bad argument matching. This should be an IP-looking string");
        let srv_kind = ip.whois_server().kind;
        fetch_with_referrals( _fetch_ip, &ip, srv_kind)
    }
}

pub fn fetch_ip(ip: &IpAddr) -> Result<Info, Box<dyn Error>> {
    let srv_kind = ip.whois_server().kind;
    fetch_with_referrals( _fetch_ip, &ip, srv_kind)
}

impl QueryParam for IpAddr {}

/// Opens a TCP connection to the whois server and
/// asks for the indicated ip address.
///
/// # Returns
/// Result with either:
/// - Ok containing all the response in a string
/// - Error
///
/// # Panics
/// If it can't write the query to the socket
/// FIXME: Should produce an Err result instead.
///
/// # TODO
/// Consider generalizing this in the query module.
/// This code is not tied much to the ip addr query case.
fn _fetch_ip(ip: &IpAddr, server: &Option<Server>) -> Result<String, Box<dyn Error>> {

    // Guess whose server this ip is
    let aux: Server;
    let srv: &Server = match server {
        Some(ref _srv) => _srv ,
        None => {
            aux = ip.whois_server();
            &aux
        },
    };

    // Prepare query
        // Arin has a special syntax for network addresses
        // n stands for numbers and + for more info
    let query_flags;
    if srv.url == "whois.arin.net".to_string() {
        query_flags = "n + ".to_string();
    } else {
        query_flags = "".to_string();
    }

    // Resolve domain
    let ips: Vec<SocketAddr> = resolve_host(&srv.url).
        expect(&format!("Could not resolve host {}", &srv.url)).
        map(|addr| { SocketAddr::from((addr, srv.port)) }).
        collect();

    // Create socket
    let mut stream: TcpStream;

    // https://doc.rust-lang.org/book/2018-edition/ch04-03-slices.html#string-slices
    //
    // A string slice is a reference to part of a String, and it looks like this:
    // let s = String::from("hello world");
    // let hello = &s[0..5];
    //
    // ---
    //
    // https://doc.rust-lang.org/std/vec/struct.Vec.html#method.as_slice
    //
    // pub fn as_slice(&self) -> &[T]
    // Extracts a slice containing the entire vector.
    // Equivalent to &s[..].
    // TODO: log the error for accountability
    stream = TcpStream::connect(&ips[..])?;
        //expect(&format!("Could not connect to host addresses of {}", &srv_url);

    // Send query
    match stream.write(&format!("{}{}\n", query_flags, ip).into_bytes()) {
        Ok(s) => s,
        Err(e) => {
            panic![format!("Could not send query. Error: {}", e) ];
        }
    };

    // Return result
    let mut buffer = String::new();
    match stream.read_to_string(&mut buffer) {
        Ok(_s) => Ok(buffer),
        Err(e) => {
            warn!("Could not read stream. Error: {}", e);
            Err(Box::new(e))
        }
    }
}

