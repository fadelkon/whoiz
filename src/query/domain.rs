use query::FetchServer;
use info::Info;
use std::error::Error;

#[allow(dead_code)]
pub struct DomainQuery {}

#[allow(unused_variables)]
pub fn fetch_domain(domain: &str) -> Result<Info, Box<dyn Error>> {
    panic!("fetch_domain not implemented");
}

impl FetchServer for DomainQuery {
    fn fetch(s: &str) -> Result<Info, Box<dyn Error>> {
        fetch_domain(s)
    }
}
