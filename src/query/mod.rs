pub mod ip;
pub mod domain;

use info::Info;
use data::Data;
use data::IntoDataThruServerKind;
use data::ParseDataError;
use server::Server;
use server::ServerKind;
use std::error::Error;
use query::ip::IpQuery;

pub trait FetchServer {
    fn fetch(s_query: &str) -> Result<Info, Box<dyn Error>>;
}

pub trait QueryParam {
}

pub fn fetch(query: &str) -> Result<Info, Box<dyn Error>> {
    // FIXME decide which type of content it is. IP, Domain, AS, something else.
    IpQuery::fetch(query)
}

/// Parse result and fetch again to referred server.
/// Detects:
/// - ReferralServer
/// - referto
///
/// # TODO
/// Use the server kind information to detect one or another token.
pub fn fetch_with_referrals<T>(fetch_fn: fn(&T, &Option<Server>) -> Result<String, Box<dyn Error>>, t_query: &T, srv_kind: ServerKind) -> Result<Info, Box<dyn Error>>
    where T: QueryParam {

    let mut raw: String;
    let mut cumulative: String;
    let mut referral: Option<Server> = None;
    let mut redirect_count: u8 = 0;
    let mut last_srv_kind = srv_kind.clone();

    match fetch_fn(&t_query, &referral) {
        Ok(s) => raw = s,
        Err(_e) => return Err(Box::new(ParseDataError))
    };
    cumulative = raw.clone();
    // TODO: here we already know the whois server we asked to, so
    // we should be able to use that info to do `parse_kind(srv_kind)`
    referral = raw.parse::<Data>()?.referral();

    // While some referral is found, follow it, until N times.
    while match referral { None => false, _ => true } {
        raw = match fetch_fn(&t_query, &referral) {
            Ok(s) => s,
            // TODO: log the error for accountability
            Err(_e) => break,
        };
        cumulative = cumulative +
            &format!("\n\n# INFO: following referral to {:?}\n\n", &referral) +
            &raw;
        if let Some(r) = &referral { last_srv_kind = r.kind.clone() };
        referral = raw.parse::<Data>()?.referral();
        redirect_count = redirect_count + 1;
        if redirect_count == 10 {
            panic!["Too many referrals found. Probably a bug"];
        }
    }
    // Parses all responses with the same srv kind
    let data = match cumulative.parse_whois::<Data>(&last_srv_kind) {
        Ok(a) => a,
        Err(e) => return Err(Box::new(e)),
    };

    // Parses all responses with the same srv kind
    match data.parse_whois::<Info>(&last_srv_kind) {
        Ok(a) => Ok(a),
        Err(e) => return Err(Box::new(e)),
    }
}


