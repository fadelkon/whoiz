//! Relates IP addresses with their manager entities.
//! This is a rather short list, as many subnets have been reassigned
//! and transferred. However, top level whois servers redirect us to those ones.
//!
//! # Types of internet adresses registry
//! *RIR* Regional Internet Registry. 5 continents
//! *LIR* Local Internet Registry. Many of them. ISP, unis, etc.
//! *NIR* National Internet Registry. 7 in APNIC and 2 in LACNIC.

use std::net::{IpAddr, Ipv4Addr, Ipv6Addr};
use server::Server;
use server::ServerKind;
use server::RirId;

pub trait ManagedByWhoisServer {
    fn whois_server(&self) -> Server;
    fn whois_kind(&self) -> ServerKind;
}

impl ManagedByWhoisServer for IpAddr {
    fn whois_server(&self) -> Server {
        match self {
            IpAddr::V4(ip) => ip.whois_server(),
            IpAddr::V6(ip) => ip.whois_server(),
        }
    }
    fn whois_kind(&self) -> ServerKind {
        match self {
            IpAddr::V4(ip) => ip.whois_kind(),
            IpAddr::V6(ip) => ip.whois_kind(),
        }
    }
}

impl ManagedByWhoisServer for Ipv6Addr {

    fn whois_server(&self) -> Server {
        panic!["Queries for IP addresses V6 are not implemented"];
    }
    fn whois_kind(&self) -> ServerKind {
        panic!["Queries for IP addresses V6 are not implemented"];
    }
}
impl ManagedByWhoisServer for Ipv4Addr {
    fn whois_server(&self) -> Server {
       Server::new_rir(self.rir())
    }
    fn whois_kind(&self) -> ServerKind {
        ServerKind::Rir(self.rir())
    }
}

pub trait ManagedByRir: ManagedByWhoisServer {
    fn rir(&self) -> RirId;
}

impl ManagedByRir for Ipv4Addr{
    fn rir(&self) -> RirId {
        match self.octets()[0] {
            // RESERVED - 0 The address 0.0.0.0 may only be used as the address of an outgoing packet
            // when a computer is learning which IP address it should use.  It is never used as a
            // destination address.  Addresses starting with "0." are sometimes used for broadcasts to
            // directly connected devices.
            000 => RirId::Arin,
            001 => RirId::Apnic,
            002 => RirId::Ripe,
            003 => RirId::Arin,
            004 => RirId::Arin,
            005 => RirId::Ripe,
            006 => RirId::Arin,
            007 => RirId::Arin,
            008 => RirId::Arin,
            009 => RirId::Arin,
            // RESERVED - 10 They are only intended for use within a private context  and traffic that
            // needs to cross the Internet will need to use a different, unique address.
            010 => RirId::Arin,
            011 => RirId::Arin,
            012 => RirId::Arin,
            013 => RirId::Arin,
            014 => RirId::Apnic,
            015 => RirId::Arin,
            016 => RirId::Arin,
            017 => RirId::Arin,
            018 => RirId::Arin,
            019 => RirId::Arin,
            020 => RirId::Arin,
            021 => RirId::Arin,
            022 => RirId::Arin,
            023 => RirId::Arin,
            024 => RirId::Arin,
            025 => RirId::Ripe,
            026 => RirId::Arin,
            027 => RirId::Apnic,
            028 => RirId::Arin,
            029 => RirId::Arin,
            030 => RirId::Arin,
            031 => RirId::Ripe,
            032 => RirId::Arin,
            033 => RirId::Arin,
            034 => RirId::Arin,
            035 => RirId::Arin,
            036 => RirId::Apnic,
            037 => RirId::Ripe,
            038 => RirId::Arin,
            039 => RirId::Apnic,
            040 => RirId::Arin,
            041 => RirId::Afrinic,
            042 => RirId::Apnic,
            043 => RirId::Apnic,
            044 => RirId::Arin,
            045 => RirId::Arin,
            046 => RirId::Ripe,
            047 => RirId::Arin,
            048 => RirId::Arin,
            049 => RirId::Apnic,
            050 => RirId::Arin,
            051 => RirId::Ripe,
            052 => RirId::Arin,
            053 => RirId::Ripe,
            054 => RirId::Arin,
            055 => RirId::Arin,
            056 => RirId::Arin,
            057 => RirId::Ripe,
            058 => RirId::Apnic,
            059 => RirId::Apnic,
            060 => RirId::Apnic,
            061 => RirId::Apnic,
            062 => RirId::Ripe,
            063 => RirId::Arin,
            064 => RirId::Arin,
            065 => RirId::Arin,
            066 => RirId::Arin,
            067 => RirId::Arin,
            068 => RirId::Arin,
            069 => RirId::Arin,
            070 => RirId::Arin,
            071 => RirId::Arin,
            072 => RirId::Arin,
            073 => RirId::Arin,
            074 => RirId::Arin,
            075 => RirId::Arin,
            076 => RirId::Arin,
            077 => RirId::Ripe,
            078 => RirId::Ripe,
            079 => RirId::Ripe,
            080 => RirId::Ripe,
            081 => RirId::Ripe,
            082 => RirId::Ripe,
            083 => RirId::Ripe,
            084 => RirId::Ripe,
            085 => RirId::Ripe,
            086 => RirId::Ripe,
            087 => RirId::Ripe,
            088 => RirId::Ripe,
            089 => RirId::Ripe,
            090 => RirId::Ripe,
            091 => RirId::Ripe,
            092 => RirId::Ripe,
            093 => RirId::Ripe,
            094 => RirId::Ripe,
            095 => RirId::Ripe,
            096 => RirId::Arin,
            097 => RirId::Arin,
            098 => RirId::Arin,
            099 => RirId::Arin,
            100 => RirId::Arin,
            101 => RirId::Apnic,
            102 => RirId::Afrinic,
            103 => RirId::Apnic,
            104 => RirId::Arin,
            105 => RirId::Afrinic,
            106 => RirId::Apnic,
            107 => RirId::Arin,
            108 => RirId::Arin,
            109 => RirId::Ripe,
            110 => RirId::Apnic,
            111 => RirId::Apnic,
            112 => RirId::Apnic,
            113 => RirId::Apnic,
            114 => RirId::Apnic,
            115 => RirId::Apnic,
            116 => RirId::Apnic,
            117 => RirId::Apnic,
            118 => RirId::Apnic,
            119 => RirId::Apnic,
            120 => RirId::Apnic,
            121 => RirId::Apnic,
            122 => RirId::Apnic,
            123 => RirId::Apnic,
            124 => RirId::Apnic,
            125 => RirId::Apnic,
            126 => RirId::Apnic,
            // RESERVED - 127 Addresses starting with "127." are used when one program needs to talk to
            // another program running on the same machine using the Internet Protocol
            127 => RirId::Arin,
            128 => RirId::Arin,
            129 => RirId::Arin,
            130 => RirId::Arin,
            131 => RirId::Arin,
            132 => RirId::Arin,
            133 => RirId::Apnic,
            134 => RirId::Arin,
            135 => RirId::Arin,
            136 => RirId::Arin,
            137 => RirId::Arin,
            138 => RirId::Arin,
            139 => RirId::Arin,
            140 => RirId::Arin,
            141 => RirId::Ripe,
            142 => RirId::Arin,
            143 => RirId::Arin,
            144 => RirId::Arin,
            145 => RirId::Ripe,
            146 => RirId::Arin,
            147 => RirId::Arin,
            148 => RirId::Arin,
            149 => RirId::Arin,
            150 => RirId::Apnic,
            151 => RirId::Ripe,
            152 => RirId::Arin,
            153 => RirId::Apnic,
            154 => RirId::Afrinic,
            155 => RirId::Arin,
            156 => RirId::Arin,
            157 => RirId::Arin,
            158 => RirId::Arin,
            159 => RirId::Arin,
            160 => RirId::Arin,
            161 => RirId::Arin,
            162 => RirId::Arin,
            163 => RirId::Apnic,
            164 => RirId::Arin,
            165 => RirId::Arin,
            166 => RirId::Arin,
            167 => RirId::Arin,
            168 => RirId::Arin,
            169 => RirId::Arin,
            170 => RirId::Arin,
            171 => RirId::Apnic,
            172 => RirId::Arin,
            173 => RirId::Arin,
            174 => RirId::Arin,
            175 => RirId::Apnic,
            176 => RirId::Ripe,
            177 => RirId::Lacnic,
            178 => RirId::Ripe,
            179 => RirId::Lacnic,
            180 => RirId::Apnic,
            181 => RirId::Lacnic,
            182 => RirId::Apnic,
            183 => RirId::Apnic,
            184 => RirId::Arin,
            185 => RirId::Ripe,
            186 => RirId::Lacnic,
            187 => RirId::Lacnic,
            188 => RirId::Ripe,
            189 => RirId::Lacnic,
            190 => RirId::Lacnic,
            191 => RirId::Lacnic,
            192 => RirId::Arin,
            193 => RirId::Ripe,
            194 => RirId::Ripe,
            195 => RirId::Ripe,
            196 => RirId::Afrinic,
            197 => RirId::Afrinic,
            198 => RirId::Arin,
            199 => RirId::Arin,
            200 => RirId::Lacnic,
            201 => RirId::Lacnic,
            202 => RirId::Apnic,
            203 => RirId::Apnic,
            204 => RirId::Arin,
            205 => RirId::Arin,
            206 => RirId::Arin,
            207 => RirId::Arin,
            208 => RirId::Arin,
            209 => RirId::Arin,
            210 => RirId::Apnic,
            211 => RirId::Apnic,
            212 => RirId::Ripe,
            213 => RirId::Ripe,
            214 => RirId::Arin,
            215 => RirId::Arin,
            216 => RirId::Arin,
            217 => RirId::Ripe,
            218 => RirId::Apnic,
            219 => RirId::Apnic,
            220 => RirId::Apnic,
            221 => RirId::Apnic,
            222 => RirId::Apnic,
            223 => RirId::Apnic,
            // RESERVED - 224 and 239 are used for IP multicast
            224 => RirId::Arin,
            225 => RirId::Arin,
            226 => RirId::Arin,
            227 => RirId::Arin,
            228 => RirId::Arin,
            229 => RirId::Arin,
            230 => RirId::Arin,
            231 => RirId::Arin,
            232 => RirId::Arin,
            233 => RirId::Arin,
            234 => RirId::Arin,
            235 => RirId::Arin,
            236 => RirId::Arin,
            237 => RirId::Arin,
            238 => RirId::Arin,
            239 => RirId::Arin,
            // RESERVED - Addresses starting with 240 or a higher number have not been allocated and
            // should not be used, apart from 255.255.255.255, which is used for "limited broadcast" on
            // a local network.
            240 => RirId::Arin,
            241 => RirId::Arin,
            242 => RirId::Arin,
            243 => RirId::Arin,
            244 => RirId::Arin,
            245 => RirId::Arin,
            246 => RirId::Arin,
            247 => RirId::Arin,
            248 => RirId::Arin,
            249 => RirId::Arin,
            250 => RirId::Arin,
            251 => RirId::Arin,
            252 => RirId::Arin,
            253 => RirId::Arin,
            254 => RirId::Arin,
            255 => RirId::Arin,
            //_ => panic!["BUG"],
        }
    }
}
