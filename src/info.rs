use std::collections::HashMap;
use data::Data;
use data::FromData;
use data::FromDataThruServerKind;
use data::ParseDataError;
use server::ServerKind;
use server::RirId;

/// Some relevant information about whois response Data
/// You can create a new Info by setting its fiels or from parsing a Data struct
pub struct Info {
    pub org: String,
    pub cc: String,
    pub desc: String,
    pub netn: String,
    /// Autonomous System - string
    /// representations: https://www.ietf.org/rfc/rfc5396.txt
    pub ass: String,
    /// Autonomous System Number
    pub asn: u32,
}


/// Implement some getters for common fields:
/// - orgid
/// - netname
/// - abuse
/// - inetnum
impl Info {

    pub fn new() -> Info {
        Info{
            cc: "".to_string(),
            netn: "".to_string(),
            org: "".to_string(),
            desc: "".to_string(),
            ass: "".to_string(),
            asn: 0,
        }
    }
    pub fn get_country(&self) -> Option<&str> {
        if self.cc == "" { None }
        else { Some(&self.cc) }
    }
    pub fn get_org_name(&self) -> Option<&str> {
        if self.org == "" { None }
        else { Some(&self.org) }
    }
    pub fn get_net_name(&self) -> Option<&str> {
        if self.netn == "" { None }
        else { Some(&self.netn) }
    }
}

impl FromDataThruServerKind for Info {
    type Err = ParseDataError;
    fn from_data_kind(d: &Data, t: &ServerKind) -> Result<Self, Self::Err>
        where Self: Sized {
        
        enum CaseStyle {
            Pascal, // PascalCase // Mostly ARIN
            Kebab, // kebab-case // All other RIR
            Camel, // camelCase
            Snake, // snake_case
            Space, // Space Case
        }

        let case = match t {
            ServerKind::Tld => return Err(ParseDataError),
            ServerKind::Generic => return Err(ParseDataError),
            ServerKind::Rir(ririd) => {
                match ririd {
                    RirId::Afrinic => CaseStyle::Kebab,
                    RirId::Apnic => CaseStyle::Kebab,
                    RirId::Arin => CaseStyle::Pascal,
                    RirId::Lacnic => CaseStyle::Kebab, // review looks like "meltcase"
                    RirId::Ripe => CaseStyle::Kebab,
                }
            }
        };
        let get_one = | args_v: Vec<&str> | {
            let mut value;
            let mut args = args_v.iter();
            loop {
                let key = args.next();
                value = match key { Some(k) => d.map.get(k.clone()), None => None };
                // stop if no keys left to try, or if a value has been found.
                if key == None || value != None { break; }
            }
            match value {
                Some(s) => s.to_string(),
                None => "".to_string()
            }
        };
        let parse_as = | ass: &str | {
            match (ass.to_string().replace("AS", "")).parse::<u32>() {
                Ok(n) => n,
                Err(_) => 0
            }
        };
        match case {
            CaseStyle::Kebab => {
                let ass = get_one(vec!["origin"]);
                Ok(Info{
                    org: get_one(vec!["org-name", "organisation"]),
                    cc: get_one(vec!["country"]),
                    desc: get_one(vec!["descr"]),
                    netn: get_one(vec!["netname"]),
                    asn: parse_as(&ass),
                    ass: ass,
                })
            },
            CaseStyle::Pascal => {
                let ass = get_one(vec!["OriginAS"]);
                Ok(Info{
                    org: get_one(vec!["OrgName", "Organization"]),
                    cc: get_one(vec!["Country"]),
                    desc: get_one(vec!["Description"]),
                    netn: get_one(vec!["NetName"]),
                    asn: parse_as(&ass),
                    ass: ass,
                })
            }
            _ => Err(ParseDataError)
        }
    }
}

impl FromData for Info {
    type Err = ParseDataError;
    fn from_data(s: &Data) -> Result<Self, Self::Err>
        where Self: Sized {

        fn extract(v: Vec<&str>, m: &HashMap<String,String>) -> String {
            for key in v {
                match m.get(key) {
                    Some(value) => return value.to_string(),
                    None => ()
                };
            }
            "".to_string()
        }
        let info = Info {
            cc: extract(vec!["Country", "country"], &s.map),
            org: extract(vec!["OrgName", "org-name"], &s.map),
            desc: extract(vec!["Description", "descr"], &s.map),
            netn: extract(vec!["NetName", "net-name"], &s.map),
            ass: extract(vec!["origin", "OriginAS"], &s.map),
            asn: 0,
        };
        Ok(info)
    }
}
