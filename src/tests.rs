#![allow(unused_imports)]
#[cfg(test)]
use server::{Server, ServerKind, RirId};
use info::Info;
use std::net::{IpAddr, Ipv4Addr, Ipv6Addr};
use ip_map::{ManagedByWhoisServer, ManagedByRir};
use std::str::FromStr;
use query::ip::fetch_ip;


#[test]
fn ip_map_v4_server() {
    assert_eq!(IpAddr::from_str("1.2.3.4"    ).expect("bad IP addr format").whois_server(), Server::new_rir(RirId::Apnic));
    assert_eq!(IpAddr::from_str("127.0.10.1" ).expect("bad IP addr format").whois_server(), Server::new_rir(RirId::Arin));
}
#[test]
fn ip_map_v4_server_kind() {
    assert_eq!(IpAddr::from_str("147.83.2.3" ).expect("bad IP addr format").whois_kind(), ServerKind::Rir(RirId::Arin));
}
#[test]
fn ip_map_v4_rir() {
    assert_eq!(Ipv4Addr::from_str("192.168.1.0").expect("bad IP addr format").rir(), RirId::Arin);
    assert_eq!(Ipv4Addr::from_str("41.41.0.0"  ).expect("bad IP addr format").rir(), RirId::Afrinic);
    assert_eq!(Ipv4Addr::from_str("141.41.0.0" ).expect("bad IP addr format").rir(), RirId::Ripe);
    assert_eq!(Ipv4Addr::from_str("177.0.0.0"  ).expect("bad IP addr format").rir(), RirId::Lacnic);
}

#[test]
fn ip_to_str() {
    assert_eq!(format!("{}",IpAddr::from_str("192.168.1.1").unwrap()), "192.168.1.1");
}

#[test]
fn test_fetch_ip() {
    //assert!(  fetch_ip( &IpAddr::from_str("8.8.8.8").expect("bad IP addr format") ).expect("fetch problem").org == "Google LLC (GOGL)"  );
    assert!(  fetch_ip( &IpAddr::from_str("8.8.8.8").expect("bad IP addr format") ).expect("fetch problem").org == "Google LLC"  );
}

#[test]
fn test_referral_fetch_ip() {
    // Currently this fails because NetName comes before netname and then, RIPE-ERX-147-83-0-0
    // prevails over UPCNET, even if in this case, netname is more precise information.
    // The way we should be parsing is, for each server reply, parse that, and then, ask for more
    // info, instead of getting all info and afterwards parsing. This way, we'd have the serverkind
    // info at the moment, without needing to store the "query route" we've followed, in order to
    // parse each chunk with the syntax that is written in.
    // Also, it's important to store the info we extract in multiple, priorized maps. So that we
    // can first look at the bottom of the wois reply (the most concise), then the previous one,
    // and so on until we find the field we're looking for.
    let info = fetch_ip( &IpAddr::from_str("147.83.2.3").expect("bad IP addr format") ).expect("fetch problem");
    assert!( info.netn == "UPCNET", "netname  was: {}", info.netn  )
}

#[test]
fn test_org_name() {
    // hubzilla.org
    assert_eq!( Some("netcup GmbH"), fetch_ip( &IpAddr::from_str("37.120.172.168").expect("bad IP addr format")).unwrap().get_org_name()  );
    // www.ceramicafasinpat.com.ar FaSinPat, Argentina
    assert_eq!( None, fetch_ip( &IpAddr::from_str("200.58.120.110").expect("bad IP addr format")).expect("fetch_problem").get_org_name()  );
}

#[test]
fn test_country() {
    // www.ceramicafasinpat.com.ar FaSinPat, Argentina
    assert_eq!( Some("AR"), fetch_ip( &IpAddr::from_str("200.58.120.110").expect("bad IP addr format")).unwrap().get_country());
    // gensokoyo.town Mastodon instance from Japan
    assert_eq!( Some("JP"), fetch_ip( &IpAddr::from_str("45.77.131.253").expect("bad IP addr format")).unwrap().get_country());
    // mastodon.art
    assert_eq!( Some("NL"), fetch_ip( &IpAddr::from_str("94.237.40.128").expect("bad IP addr format")).unwrap().get_country());

}
