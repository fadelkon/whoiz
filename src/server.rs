//! Common properties shared by whois servers.
//! Implemented for known ones like RIR, and
//! in a future maybe for top level domains.

use std::fmt::Debug;
use std::str::FromStr;
use std::fmt;

#[derive(Clone)]
#[derive(PartialEq)]
#[derive(Debug)]
pub enum RirId {
    Afrinic,
    Arin,
    Apnic,
    Lacnic,
    Ripe,
}

#[derive(Clone)]
#[derive(PartialEq)]
#[derive(Debug)]
pub enum ServerKind {
    Generic,
    Rir(RirId),
    Tld,
}

#[derive(PartialEq)]
#[derive(Debug)]
pub struct Server {
    pub port: u16,
    pub url: String,
    pub kind: ServerKind,
}

impl Server {
    pub fn new_rir(rir_id: RirId) -> Self {
        Server {
            port: 43,
            url: {
                let x = match &rir_id {
                    RirId::Afrinic => "whois.afrinic.net",
                    RirId::Arin => "whois.arin.net",
                    RirId::Apnic => "whois.apnic.net",
                    RirId::Lacnic => "whois.lacnic.net",
                    RirId::Ripe => "whois.ripe.net",
                };
                x.to_string()
            },
            kind: ServerKind::Rir(rir_id),
        }
    }
}

impl FromStr for Server {
    type Err = ParseServerError;
    /// url is expected to be something like:
    /// `rwhois://whois.domain.com:4321`
    /// `whois.arin.net`
    /// `whois://domain.tld`
    ///
    /// # Returns
    /// A triplet of the form `(proto:&str, hostname:&str, port:u16)`
    ///
    /// # Panics
    /// If the port number specified can't be parsed into an u16
    /// FIXME: Should return a Result instead of panic'ing.
    fn from_str(url: &str) -> Result<Self, Self::Err> {
        let mut proto: &str = "";
        let mut iter =  url.split("://");
        if url.contains("://") {
            //  consumes proto spit and leaves the rest
            proto = iter.next().unwrap();
        }
        // no more proto part, both if proto was specified or not
        let mut iter2 = iter.next().unwrap().split(":");
        let domain = iter2.next().unwrap();
        let port = match iter.next() {
            None => {
                match proto {
                    ""       => 43,
                    "rwhois" => 4321,
                    "whois"  => 43,
                    "http"   => 80,
                    "https"  => 443,
                    "ftp"    => 21,
                    _        => 43,
                }
            },
            Some(text) => text.parse().unwrap_or(43)
        };
        // let's try to guess serverkind, if known
        let kind = match domain {
            "whois.afrinic.net" => ServerKind::Rir(RirId::Afrinic),
            "whois.arin.net"    => ServerKind::Rir(RirId::Arin),
            "whois.apnic.net"   => ServerKind::Rir(RirId::Apnic),  
            "whois.lacnic.net"  => ServerKind::Rir(RirId::Lacnic),
            "whois.ripe.net"    => ServerKind::Rir(RirId::Ripe),
            _ => ServerKind::Generic,

        };
        Ok(Server{port: port, url: domain.to_string(), kind: kind})
    }
}

// std/error/trait.Error.html
//
#[derive(Debug)]
pub struct ParseServerError;


impl fmt::Display for ParseServerError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(f, "Error while parsing whois server string")
        }
}

impl std::error::Error for ParseServerError {
}
