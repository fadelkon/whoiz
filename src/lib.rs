#[macro_use] extern crate log;
extern crate resolve;

mod ip_map;
mod query;
mod tests;
mod parser;
mod server;
mod info;
mod data;

pub use info::Info;
pub use data::ParseDataError;
pub use query::fetch;
pub use query::ip::fetch_ip;
